var express = require('express');
var router = express.Router();

/* GET files listing. */
router.get('/files', function(req, res, next) {
  res.send('respond with a file');
});

module.exports = router;
